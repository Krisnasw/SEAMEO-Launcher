package intivestudio.web.id.seameolauncher;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by outattacker on 29/04/16.
 */
public class Priority extends AppCompatActivity {

    final Context context = this;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_priority);

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("7 Priority Areas");

        ImageView satu = (ImageView) findViewById(R.id.img1);
        ImageView dua = (ImageView) findViewById(R.id.img2);
        ImageView tiga = (ImageView) findViewById(R.id.img3);
        ImageView empat = (ImageView) findViewById(R.id.tujuh);
        ImageView lima = (ImageView) findViewById(R.id.img5);
        ImageView enam = (ImageView) findViewById(R.id.img6);
        ImageView tujuh = (ImageView) findViewById(R.id.img7);
        ImageView lapan = (ImageView) findViewById(R.id.img8);

        satu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.curoom);
                dialog.setTitle("Info : ");
                dialog.show();

                TextView text = (TextView) dialog.findViewById(R.id.textt);
                text.setText("Achieving universal pre-primary education\n" +
                        "by 2035, with particular target on the\n" +
                        "disadvantaged, such as poor children; rural\n" +
                        "communities; marginalised ethnic and linguistic\n" +
                        "communities; and children with disabilities\n" +
                        "benefiting the most.");

                ImageView imeg = (ImageView) dialog.findViewById(R.id.dialogButtonOK);
                imeg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        dua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.curoom);
                dialog.setTitle("Info : ");
                dialog.show();

                TextView text = (TextView) dialog.findViewById(R.id.textt);
                text.setText("Addressing barriers to inclusion and\n" +
                        "access to basic learning opportunities\n" +
                        "of all learners through innovations in\n" +
                        "education delivery and management");

                ImageView imeg = (ImageView) dialog.findViewById(R.id.dialogButtonOK);
                imeg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        tiga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.curoom);
                dialog.setTitle("Info : ");
                dialog.show();

                TextView text = (TextView) dialog.findViewById(R.id.textt);
                text.setText("Preparing schools leaders, teachers, students, and local\n" +
                        "communities in managing and maintaining the delivery\n" +
                        "of education services during emergencies such as\n" +
                        "conflicts, extreme weather, and natural disasters.");

                ImageView imeg = (ImageView) dialog.findViewById(R.id.dialogButtonOK);
                imeg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        empat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "7 Priority Areas", Toast.LENGTH_LONG).show();
            }
        });

        lima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.curoom);
                dialog.setTitle("Info : ");
                dialog.show();

                TextView text = (TextView) dialog.findViewById(R.id.textt);
                text.setText("Promoting TVET among learners, teachers and parents with more\n" +
                        "visible investments and relevant curricula that focus on creativity and\n" +
                        "innovation, with a clear pathway to lifelong learning, higher education\n" +
                        "and regional labour, skill and learners’ mobility.");

                ImageView imeg = (ImageView) dialog.findViewById(R.id.dialogButtonOK);
                imeg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        enam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.curoom);
                dialog.setTitle("Info : ");
                dialog.show();

                TextView text = (TextView) dialog.findViewById(R.id.textt);
                text.setText("Pursuing a radical reform through systematic analysis\n" +
                        "of knowledge, skills, and values needed to effectively\n" +
                        "respond to changing global contexts, particularly\n" +
                        "to the ever-increasing complexity of the Southeast\n" +
                        "Asian economic, socio-cultural, and political environment,\n" +
                        "developing teacher imbued with ASEAN ideals\n" +
                        "in building ASEAN community.");

                ImageView imeg = (ImageView) dialog.findViewById(R.id.dialogButtonOK);
                imeg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        tujuh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.curoom);
                dialog.setTitle("Info : ");
                dialog.show();

                TextView text = (TextView) dialog.findViewById(R.id.textt);
                text.setText("Institutional-level harmonisation taking place with Member\n" +
                        "Countries investing in strengthening higher education\n" +
                        "institutions with each institutions determining their most\n" +
                        "important needs, supported by research, in order to be\n" +
                        "able to co-ordinate and set quality standards with other\n" +
                        "institutions.");

                ImageView imeg = (ImageView) dialog.findViewById(R.id.dialogButtonOK);
                imeg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        lapan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.curoom);
                dialog.setTitle("Info : ");
                dialog.show();

                TextView text = (TextView) dialog.findViewById(R.id.textt);
                text.setText("Making teaching a first choice profession through\n" +
                        "comprehensive, strategic, and practice-based reforms of\n" +
                        "teacher management and development systems through\n" +
                        "more professional preparation at pre-service and in-service\n" +
                        "processes, following an explicit and shared teacher\n" +
                        "competency framework and a set of standards applicable\n" +
                        "across the region.");

                ImageView imeg = (ImageView) dialog.findViewById(R.id.dialogButtonOK);
                imeg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }
}
