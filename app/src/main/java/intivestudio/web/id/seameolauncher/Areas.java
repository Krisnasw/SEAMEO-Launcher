package intivestudio.web.id.seameolauncher;

import java.util.ArrayList;

import android.os.Bundle;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import android.view.View;
import android.widget.ListView;

/**
 * Created by outattacker on 29/04/16.
 */
public class Areas extends AppCompatActivity {

    Toolbar toolbar;
    ListView lv;
    Context context;
    ArrayList prgmName;

    public static int [] prgmImages={R.mipmap.biotrop, R.mipmap.cell, R.mipmap.chat, R.mipmap.inotech, R.mipmap.qil, R.mipmap.qim, R.mipmap.qis,
            R.mipmap.recfon, R.mipmap.recsam, R.mipmap.relc, R.mipmap.retrac, R.mipmap.rihed, R.mipmap.seamolec};

    public static String [] prgmNameList={"SEAMEO BIOTROP", "SEAMEO CELL", "SEAMEO CHAT", "SEAMEO INOTECH",
            "SEAMEO Qitep In Languange", "SEAMEO Qitep in Mathematics", "SEAMEO Qitep In Science", "SEAMEO RECFON",
            "SEAMEO RECSAM", "SEAMEO RELC", "SEAMEO RETRAC", "SEAMEO RIHED", "SEAMEO SEAMOLEC"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_areas);

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("21 SEAMEO CENTRE");

        context=this;

        lv=(ListView) findViewById(R.id.android_list);
        lv.setAdapter(new MyBaseAdapter(Areas.this, prgmNameList,prgmImages));
    }

//    public boolean onCreateOptionsMenu(View view) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getLayoutInflater().inflate(R.layout.rowmodel, null);
//        return true;
//    }

}
