package intivestudio.web.id.seameolauncher;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by outattacker on 29/04/16.
 */

public class MyBaseAdapter extends BaseAdapter {
    String [] result;
    Context context;
    int [] imageId;
    private static LayoutInflater inflater=null;
    public MyBaseAdapter(Areas areas, String[] prgmNameList, int[] prgmImages) {
        // TODO Auto-generated constructor stub
        result=prgmNameList;
        context=areas;
        imageId=prgmImages;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv;
        ImageView img;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.rowmodel, null);
        holder.tv=(TextView) rowView.findViewById(R.id.nameTv);
        holder.img=(ImageView) rowView.findViewById(R.id.imageView1);
        holder.tv.setText(result[position]);
        holder.img.setImageResource(imageId[position]);
        rowView.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                // TODO Auto-generated method stub

                String jeneng = "" +result[position];

                Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Info : ");
                dialog.show();
                System.out.println("ID : " +position);
                System.out.println("Jeneng : " +jeneng);

                TextView tv = (TextView) dialog.findViewById(R.id.textxxx);
                ImageView img = (ImageView) dialog.findViewById(R.id.imagexxx);
                tv.setText(jeneng);
                img.setImageResource(imageId[position]);
                ImageView bt = (ImageView) dialog.findViewById(R.id.dialogButtonOK);

                if (position == 0) {

                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                                Intent q = new Intent(context, Biotrop.class);
                                context.startActivity(q);
                        }
                    });

                } else if (position == 1) {

                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Cell.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 2) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, SEAChat.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 3) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Inotech.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 4) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Seaqil.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 5) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Seaqim.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 6) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Seaqis.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 7) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Recfon.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 8) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Recsam.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 9) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Relc.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 10) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Retrac.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 11) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Rihed.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 12) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Seamolec.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 13) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Searca.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 14) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Sen.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 15) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Spafa.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 16) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Tropnet.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 17) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Tropmalay.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 18) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Tropinoy.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 19) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Tropthai.class);
                            context.startActivity(q);
                        }
                    });
                } else if (position == 20) {
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent q = new Intent(context, Voctech.class);
                            context.startActivity(q);
                        }
                    });
                }
                else
                {
                    Toast.makeText(context, "Tidak Ada Data", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });
        return rowView;
    }

}